-include ./conf/config.mk
ifndef PREX_SRC
ifdef MAKECMDGOALS
$(MAKECMDGOALS):
else
all:
endif
	@echo "Error: Please run configure at the top of source tree"
	exit 1
else
SUBDIRS	= boot dev sys user mk
CLEANS = conf/config.h conf/config.mk
include $(PREX_SRC)/mk/Makefile.inc
endif
