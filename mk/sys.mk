#
# Make options for kernel
#
INC_FLAGS = -I$(PREX_SRC)/conf \
	    -I$(PREX_SRC)/sys/arch/$(PREX_ARCH)/include \
	    -I$(PREX_SRC)/sys/include

ASFLAGS = $(INC_FLAGS)
CPPFLAGS := -D__KERNEL__ $(INC_FLAGS)
CFLAGS := $(CPPFLAGS) -nostdinc -fno-builtin
LDFLAGS = -static -nostdlib

ifeq ($(KTRACE),1)
CFLAGS += -finstrument-functions
endif

-include $(PREX_SRC)/sys/arch/$(PREX_ARCH)/$(PREX_PLATFORM)/sys.mk
include $(PREX_SRC)/mk/Makefile.inc
