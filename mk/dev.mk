#
# Make options for driver module
#
INC_FLAGS = -I$(PREX_SRC)/conf \
	-I$(PREX_SRC)/dev/arch/$(PREX_ARCH)/include \
	-I$(PREX_SRC)/dev/include

ASFLAGS = $(INC_FLAGS)
CPPFLAGS := -D__DRIVER__ $(INC_FLAGS)
CFLAGS := $(CPPFLAGS) -nostdinc -fno-builtin
LDFLAGS = -static -nostdlib -r

ifeq ($(KTRACE),1)
CFLAGS += -finstrument-functions
endif

-include $(PREX_SRC)/dev/arch/$(PREX_ARCH)/$(PREX_PLATFORM)/dev.mk
include $(PREX_SRC)/mk/Makefile.inc
