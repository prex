#
# Make options for Boot Loader
#
INC_FLAGS = -I$(PREX_SRC)/conf \
	-I$(PREX_SRC)/boot/arch/$(PREX_ARCH)/include \
	-I$(PREX_SRC)/boot/include

ASFLAGS = $(INC_FLAGS)
CPPFLAGS := -D__BOOT__ $(INC_FLAGS)
CFLAGS := $(CPPFLAGS) -nostdinc -fno-builtin
LDFLAGS = -static -nostdlib

-include $(PREX_SRC)/boot/arch/$(PREX_ARCH)/$(PREX_PLATFORM)/boot.mk
include $(PREX_SRC)/mk/Makefile.inc
